#![allow(dead_code)]

use rand::{random, thread_rng, Rng};
use std::cmp::Ordering;
use std::collections::VecDeque;

/// Takes a random element from l and pushes it in p.
/// Returns a pair of vectors : l (with said element removed) and p (with said element added).
fn extraction_alea<'a>(
    l: &'a mut Vec<usize>,
    p: &'a mut Vec<usize>,
) -> (&'a mut Vec<usize>, &'a mut Vec<usize>) {
    let mut rng = thread_rng();
    let r = rng.gen_range(0, l.len());
    p.push(l.remove(r));
    (l, p)
}

/// Generates a random permutation of integers from 1 to n (included).
pub fn gen_permutation(n: usize) -> Vec<usize> {
    let mut l = (1..(n + 1)).collect::<Vec<usize>>();
    let mut p = vec![];
    while p.len() != n {
        extraction_alea(&mut l, &mut p);
    }
    p
}

/// Randomly insert the first vector in the second.
fn intercale<'a>(
    l1: &'a mut VecDeque<usize>,
    l2: &'a mut VecDeque<usize>,
) -> &'a mut VecDeque<usize> {
    fn intercale_aux<'a>(
        l1: &'a mut VecDeque<usize>,
        l2: &'a mut VecDeque<usize>,
        i: usize,
    ) -> &'a mut VecDeque<usize> {
        if i == l2.len() || l1.is_empty() {
            l2.append(l1);
            l2
        } else {
            let threshold: f64 = (l1.len() as f64) / (l1.len() + l2.len()) as f64;
            if random::<f64>() < threshold {
                // unwrap() cannot fail as we tested l1.is-empty() earlier.
                // We use VecDeque so we can use pop_front()
                l2.insert(i, l1.pop_front().unwrap());
            }
            intercale_aux(l1, l2, i + 1)
        }
    };
    intercale_aux(l1, l2, 0)
}

/// Generates a random permutation of integers from p to q (included).
pub fn gen_permutation2(p: usize, q: usize) -> VecDeque<usize> {
    match p.cmp(&q) {
        Ordering::Greater => VecDeque::new(),
        Ordering::Equal => vec![p].into_iter().collect(),
        Ordering::Less => {
            let mut v1 = gen_permutation2(p, (p + q) / 2);
            let mut v2 = gen_permutation2((p + q) / 2 + 1, q);
            intercale(&mut v1, &mut v2);
            v2
        }
    }
}

/// A binary tree.
#[derive(Debug, PartialEq, Clone)]
pub enum Tree {
    Leaf,
    Node {
        label: usize,
        left: Box<Tree>,
        right: Box<Tree>,
        size: usize,
        lock: bool,
        signature: Option<String>,
    },
    CNode {
        tab: Vec<usize>,
        model: Box<Tree>,
    },
}

impl Tree {
    /// Makes a new tree with a root and two leafs.
    fn new(label: usize) -> Tree {
        Tree::Node {
            label,
            left: Box::new(Tree::Leaf),
            right: Box::new(Tree::Leaf),
            size: 1,
            lock: false,
            signature: Some("()".to_string()),
        }
    }

    /// Returns a tree containing the list's elements, inserted in order.
    /// Consumes the vector.
    pub fn from_list(mut v: Vec<usize>) -> Tree {
        let mut t = Tree::Leaf;
        v.reverse();
        while let Some(n) = v.pop() {
            t = t.insert(n);
        }
        t
    }

    /// Prints the tree in infix notation.
    pub fn print(&self) {
        fn print_aux(t: &Tree) -> String {
            match t {
                Tree::Leaf => format!("()"),
                Tree::Node {
                    label, left, right, ..
                } => format!("({} {} {})", print_aux(left), label, print_aux(right))
                    /*format!("({} -- {}\n |\n {})", label, print_aux(right), print_aux(left))*/,
                Tree::CNode { model, tab } => format!("(C {:?} : {})", tab, model.get_label().unwrap()),
            }
        }
        println!("{}", print_aux(&self))
    }

    /// Returns a tree's label, or () if it's a leaf.
    fn get_label(&self) -> Result<usize, ()> {
        match self {
            Tree::Node { label, .. } => Result::Ok(*label),
            _ => Result::Err(()),
        }
    }

    /// Returns a tree's left subtree, or () if it's a leaf.
    fn get_left(&self) -> Result<&Tree, ()> {
        match self {
            Tree::Node { left, .. } => Result::Ok(left),
            _ => Result::Err(()),
        }
    }

    /// Returns a tree's right subtree, or () if it's a leaf.
    fn get_right(&self) -> Result<&Tree, ()> {
        match self {
            Tree::Node { right, .. } => Result::Ok(right),
            _ => Result::Err(()),
        }
    }

    /// Returns a tree's size.
    fn get_size(&self) -> usize {
        match self {
            Tree::Leaf => 0,
            Tree::Node { size, .. } => *size,
            Tree::CNode { model, .. } => model.get_size(),
        }
    }

    // TODO: This doesn't work, we should make Node an independent struct and reference it in Tree to modify its fields
    /// Locks the tree so no insertion can be performed on it.
    /// Useful if a CNode points to the current tree, as we don't want its structure to change.
    fn lock(&mut self) {
        if let Tree::Node { mut lock, .. } = self {
            lock = true
        }
    }

    /// Returns true if n is in t, false otherwise.
    pub fn search(&self, n: usize) -> bool {
        fn search_aux(n: usize, tab: &[usize], model: &Tree) -> bool {
            if tab.is_empty() {
                false
            } else {
                match n.cmp(tab.first().unwrap()) {
                    Ordering::Equal => true,
                    Ordering::Less => search_aux(
                        n,
                        &tab[1..model.get_left().unwrap().get_size() + 1].to_vec(),
                        model.get_left().unwrap(),
                    ),
                    Ordering::Greater => search_aux(
                        n,
                        &tab[model.get_left().unwrap().get_size() + 1..].to_vec(),
                        model.get_right().unwrap(),
                    ),
                }
            }
        }
        match self {
            Tree::Leaf => false,
            Tree::Node {
                label, left, right, ..
            } => match n.cmp(&label) {
                Ordering::Less => left.search(n),
                Ordering::Greater => right.search(n),
                Ordering::Equal => true,
            },
            Tree::CNode { tab, model } => search_aux(n, tab, model),
        }
    }

    /// Returns a string describing t's structure.
    /// If l is t's left subtree and r is its right one, this function returns :
    /// * an empty string if t is a leaf;
    /// * "(`tree_signature(l)`)`tree_signature(r)`" otherwise.
    fn signature(&self) -> String {
        match self {
            Tree::Leaf => String::new(),
            Tree::CNode { model, .. } => model.signature(),
            Tree::Node {
                left,
                right,
                signature,
                ..
            } => {
                if let Some(s) = signature {
                    s.to_string()
                } else {
                    format!("({}){}", left.signature(), right.signature())
                }
            }
        }
    }

    /// Returns a vector containing t's elements in prefix order.
    fn prefix(&self) -> Vec<usize> {
        match self {
            Tree::Leaf => Vec::new(),
            Tree::Node {
                label, left, right, ..
            } => [vec![*label], left.prefix(), right.prefix()].concat(),
            Tree::CNode { tab, .. } => tab.to_vec(),
        }
    }

    /// Inserts n where it belongs in t :
    /// * if t is a leaf, it is replaced by a new tree with n as its root and two leafs;
    /// * if n is smaller than t's root, it is inserted in the left sub-tree;
    /// * if n is smaller than t's root, it is inserted in the right sub-tree;
    /// * otherwise, n is equal to t's root and nothing is done.
    fn insert(self, n: usize) -> Tree {
        fn insert_aux(t: Tree, n: usize) -> Result<Tree, Tree> {
            match t {
                Tree::Leaf => Ok(Tree::new(n)),
                Tree::Node {
                    label,
                    left,
                    right,
                    size,
                    lock,
                    signature,
                } => {
                    if lock {
                        Err(Tree::Node {
                            label,
                            left,
                            right,
                            size,
                            lock,
                            signature,
                        })
                    } else {
                        match n.cmp(&label) {
                            Ordering::Less => {
                                let newleft = insert_aux(*left, n);
                                if let Ok(v) = newleft {
                                    Ok(Tree::Node {
                                        label,
                                        left: Box::new(v),
                                        right,
                                        size: size + 1,
                                        lock,
                                        signature: None,
                                    })
                                } else {
                                    Err(Tree::Node {
                                        label,
                                        left: Box::new(newleft.unwrap_err()),
                                        right,
                                        size,
                                        lock,
                                        signature,
                                    })
                                }
                            }
                            Ordering::Greater => {
                                let newright = insert_aux(*right, n);
                                if let Ok(v) = newright {
                                    Ok(Tree::Node {
                                        label,
                                        left,
                                        right: Box::new(v),
                                        size: size + 1,
                                        lock,
                                        signature: None,
                                    })
                                } else {
                                    Err(Tree::Node {
                                        label,
                                        left,
                                        right: Box::new(newright.unwrap_err()),
                                        size,
                                        lock,
                                        signature,
                                    })
                                }
                            }
                            Ordering::Equal => Err(Tree::Node {
                                label,
                                left,
                                right,
                                size,
                                lock,
                                signature,
                            }),
                        }
                    }
                }
                Tree::CNode { tab, model } => Err(Tree::CNode { tab, model }),
            }
        }
        match insert_aux(self, n) {
            Ok(v) => v,
            Err(v) => v,
        }
    }

    // TODO: cloning everything seems bad, find a way not to do it
    /// Compresses a tree by comparing subtrees and creating compressed nodes referencing same structure nodes
    pub fn compress(self) -> Tree {
        fn unwrap_all(r: Result<Tree, Tree>) -> Tree {
            match r {
                Ok(v) => v,
                Err(v) => v,
            }
        }
        fn compress_aux(t: Tree, mut model: Box<Tree>) -> Result<Tree, Tree> {
            match t.clone() {
                Tree::Leaf => Err(Tree::Leaf),
                Tree::CNode { .. } => Ok(t),
                Tree::Node {
                    label,
                    left,
                    right,
                    size,
                    lock,
                    signature,
                } => {
                    if t.signature() == model.signature() {
                        Ok(Tree::CNode {
                            tab: t.prefix(),
                            model: {
                                model.lock();
                                Box::new(*model)
                            },
                        })
                    } else {
                        match *(model.clone()) {
                            Tree::Leaf => Err(Tree::Leaf),
                            Tree::CNode { model: mmodel, .. } => compress_aux(t, mmodel),
                            Tree::Node {
                                left: ml,
                                right: mr,
                                ..
                            } => match compress_aux(t.clone(), ml) {
                                Ok(v) => Ok(v),
                                Err(_) => match compress_aux(t, mr) {
                                    Ok(v) => Ok(v),
                                    Err(_) => {
                                        let newleft = compress_aux(*left, model.clone());
                                        let newright = compress_aux(*right, model.clone());
                                        Err(Tree::Node {
                                            label,
                                            left: Box::new(unwrap_all(newleft)),
                                            right: Box::new(unwrap_all(newright)),
                                            size,
                                            lock,
                                            signature,
                                        })
                                    }
                                },
                            },
                        }
                    }
                }
            }
        }
        match self {
            Tree::Leaf => Tree::Leaf,
            Tree::Node {
                label,
                left,
                right,
                size,
                ..
            } => {
                let newleft = left.compress();
                let newright = compress_aux(*right, Box::new(newleft.clone()));
                Tree::Node {
                    label,
                    left: Box::new(newleft),
                    lock: newright.is_ok(),
                    right: Box::new(unwrap_all(newright)),
                    size,
                    signature: None,
                }
            }
            Tree::CNode { tab, model } => Tree::CNode { tab, model },
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::util::*;

    #[test]
    fn test_extraction_alea() {
        let mut l1 = vec![1, 2];
        let mut p1 = vec![3, 4];
        let (l2, p2) = extraction_alea(&mut l1, &mut p1);
        assert_eq!(l2.len(), 1);
        assert_eq!(p2.len(), 3);
        assert!(p2.contains(&1_usize) || p2.contains(&2_usize));
    }

    #[test]
    fn test_gen_permutation() {
        let perm = gen_permutation(8);
        assert_eq!(perm.len(), 8);
        assert!(perm.contains(&1_usize));
        assert!(perm.contains(&4_usize));
        assert!(perm.contains(&8_usize));
    }

    #[test]
    fn test_intercale() {
        let mut v1: VecDeque<usize> = vec![1, 2, 3].into_iter().collect();
        let mut v2: VecDeque<usize> = vec![4, 5, 6].into_iter().collect();
        let v = intercale(&mut v1, &mut v2);
        assert_eq!(v.len(), 6);
        assert!(v.contains(&2_usize));
        assert!(v.contains(&5_usize));
        print!("v : {:?}", v)
    }

    #[test]
    fn test_gen_permutation2() {
        let perm = gen_permutation2(1, 8);
        assert_eq!(perm.len(), 8);
        assert!(perm.contains(&1_usize));
        assert!(perm.contains(&4_usize));
        assert!(perm.contains(&8_usize));
    }

    #[test]
    fn test_tree_get() {
        let t1 = Tree::new(1);
        let t2 = Tree::new(2);
        let t3 = Tree::Node {
            label: 3,
            left: Box::new(t1),
            right: Box::new(t2),
            size: 3,
            lock: false,
            signature: Some("(())()".to_string()),
        };
        assert_eq!(1, t3.get_left().unwrap().get_label().unwrap());
        assert_eq!(2, t3.get_right().unwrap().get_label().unwrap());
        assert_eq!(3, t3.get_size());
    }

    #[test]
    fn test_insert() {
        let t1 = Tree::new(2);
        let t2 = Tree::new(8);
        let mut t3 = Tree::Node {
            label: 5,
            left: Box::new(t1),
            right: Box::new(t2),
            size: 3,
            lock: false,
            signature: Some("(())()".to_string()),
        };
        t3 = t3.insert(3);
        t3 = t3.insert(6);
        /*If all goes well, from the root, we should access to :
        - 3 by going left then right
        - 6 by going right then left*/
        let a = t3
            .get_left()
            .unwrap()
            .get_right()
            .unwrap()
            .get_label()
            .unwrap();
        let b = t3
            .get_right()
            .unwrap()
            .get_left()
            .unwrap()
            .get_label()
            .unwrap();
        assert_eq!(a, 3);
        assert_eq!(b, 6);
        t3.print();
    }

    #[test]
    fn test_from_list() {
        let t = Tree::from_list(vec![4, 2, 3, 8, 1, 9, 6, 7, 5]);
        let val = t
            .get_right()
            .unwrap()
            .get_left()
            .unwrap()
            .get_left()
            .unwrap()
            .get_label()
            .unwrap();
        assert_eq!(val, 5);
        t.print();
    }

    #[test]
    fn test_search() {
        let t = Tree::from_list(vec![4, 2, 3, 8, 1, 9, 6, 7, 5]);
        assert!(t.search(4));
        assert!(t.search(5));
        assert!(!t.search(10));
    }

    #[test]
    fn test_signature() {
        let t = Tree::from_list(vec![4, 2, 3, 8, 1, 9, 6, 7, 5]);
        assert_eq!(t.signature(), "((())())((())())()");
    }

    #[test]
    fn test_prefixe() {
        let t = Tree::from_list(vec![4, 2, 3, 8, 1, 9, 6, 7, 5]);
        assert_eq!(t.prefix(), [4, 2, 1, 3, 8, 6, 5, 7, 9]);
    }

    #[test]
    fn test_compress() {
        fn is_compressed(t: &Tree) -> bool {
            match t {
                Tree::CNode { .. } => true,
                _ => false,
            }
        }
        let t = Tree::from_list(vec![4, 2, 3, 8, 1, 9, 6, 7, 5]);
        let ct = t.compress();
        let l = ct.get_left().unwrap();
        let r = ct.get_right().unwrap();
        let ll = l.get_left().unwrap();
        let lr = l.get_right().unwrap();
        let rl = r.get_left().unwrap();
        let rr = r.get_right().unwrap();
        assert!(!is_compressed(ll));
        assert!(is_compressed(lr));
        assert!(is_compressed(rl));
        assert!(is_compressed(rr));
    }

    // #[test]
    // fn test_lock() {
    //     let mut t = Tree::new(3);
    //     t.lock();
    //     t = t.insert(3);
    //     t = t.insert(5);
    //     assert_eq!(t.signature(), "()");
    // }
}
